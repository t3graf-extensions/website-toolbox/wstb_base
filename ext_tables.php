<?php

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3') || die();

(static function() {

    // Make the extension configuration accessible
    $extensionConfiguration = GeneralUtility::makeInstance(
        ExtensionConfiguration::class
    );
    $websiteToolboxConfiguration = $extensionConfiguration->get('wstb_base');
    debug($websiteToolboxConfiguration);
   if ((bool) $websiteToolboxConfiguration['enableSysTemplateRecords']) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    # Disable adding new sys_template records in list module
    mod.web_list.deniedNewTables := removeFromList(sys_template)

    # Show TSconfig and tsconfig_includes fields when editing pages
    TCEFORM.pages.TSconfig.disabled=0
    TCEFORM.pages.tsconfig_includes.disabled=0
');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('
    # Hide tstemplate "Edit TypoScript record" and "Constant Editor" in core v12
    options.hideModules := removeFromList(web_typoscript_infomodify, web_typoscript_constanteditor)
');
  }
})();
