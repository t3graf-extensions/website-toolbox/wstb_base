<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Website Toolbox',
    'description' => 'Versatile, universal and very flexibly customizable website toolbox. It offers a tool for maintenance and allows automatic integration of a project sitepackage. It is modularly expandable. See the documentation for more information.',
    'category' => 'be',
    'author' => 'Development-Team',
    'author_email' => 'development@t3graf-media.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
