<?php declare(strict_types=1);

/*
 * This file is part of the package t3graf/wstb-base.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WstbBase\EventListener;

use T3graf\WstbBase\Utility\SitepackageUtility;
use TYPO3\CMS\Backend\Backend\Event\SystemInformationToolbarCollectorEvent;
use TYPO3\CMS\Backend\Backend\ToolbarItems\SystemInformationToolbarItem;
use TYPO3\CMS\Core\Utility\CommandUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * VersionToolbarItem
 */
class SitepackageToolbarItem
{
    public function __invoke(SystemInformationToolbarCollectorEvent $event): void
    {
        $this->addVersionInformation($event->getToolbarItem());
    }

    /**
     * Called by the system information toolbar signal/slot dispatch.
     *
     * @param SystemInformationToolbarItem $systemInformation
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public function addVersionInformation(SystemInformationToolbarItem $systemInformation): void
    {
        $sitepackage = (string)SitepackageUtility::getActiveSitepackage();

       // Set system information entry
        $systemInformation->addSystemInformation(
            'Sitepackage',
            htmlspecialchars($sitepackage, ENT_QUOTES | ENT_HTML5),
            'systeminformation-activesitepackage'
        );
    }
}
