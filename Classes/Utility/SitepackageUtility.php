<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WstbBase\Utility;

use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helper to render a dynamic selection of available site extensions
 * and to fetch a theme for certain page
 */
class SitepackageUtility
{
    /**
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public static function getActiveSitepackage(): ?string
    {
        $siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
        $allSites = $siteFinder->getAllSites();
        if (array_key_first($allSites) !== null) {
            $firstSite = $allSites[array_key_first($allSites)];
            $rootPageId = $firstSite->getRootPageId();
            $site = $siteFinder->getSiteByRootPageId($rootPageId);
            $configuration = $site->getConfiguration();
        }
        if (isset($configuration['sitePackage'])) {
            $activeSitepackage = (string)$configuration['sitePackage'];
        }
        if ($activeSitepackage === '') {$activeSitepackage = 'No sitepackage available';}
        return $activeSitepackage;
    }
}
