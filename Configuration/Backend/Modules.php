<?php

return [
    'toolbox' => [
        'position' => ['after' => 'web'],
        'labels' => 'LLL:EXT:wstb_base/Resources/Private/Language/locallang_mod_wstb.xlf',
        'iconIdentifier' => 'modulegroup-website-toolbox',
        'navigationComponent' => '@typo3/backend/page-tree/page-tree-element',
    ],
    'sitepackage' => [
        'parent' => 'toolbox',
        'position' => ['bottom'],
        'access' => 'user',
        'workspaces' => 'live',
        'path' => '/toolbox/sitepackage',
        'labels' => 'LLL:EXT:wstb_base/Resources/Private/Language/locallang_sitepackage.xlf',
        'iconIdentifier' => 'module-sitepackage',
        'extensionName' => 'WstbBase',
        'controllerActions' => [
            \T3graf\WstbBase\Controller\sitepackageController::class => [
                'show'
            ]
        ],
    ],

];
