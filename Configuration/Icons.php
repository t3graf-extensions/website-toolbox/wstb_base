<?php

declare(strict_types=1);

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

return [
    // Icon identifier
    'modulegroup-website-toolbox' => [
        // Icon provider class
        'provider' => SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_base/Resources/Public/Icons/modulegroup-website-toolbox.svg',
    ],
    'module-sitepackage' => [
        'provider' => SvgIconProvider::class,
        // The source bitmap file
        'source' => 'EXT:wstb_base/Resources/Public/Icons/module-sitepackage.svg',

    ],
];
